# PSFabrik


Este repositorio es un clon de [https://github.com/ikerlandajuela/PowerShell](https://github.com/ikerlandajuela/PowerShell), se continuará de ahora en adelante alojado en GitLab en [este repositorio](https://gitlab.com/soka/PSFabrik). 

**La web generada con MkDocs se puede visualizar en [https://soka.gitlab.io/PSFabrik/](https://soka.gitlab.io/PSFabrik/).**


# Enlaces externos

* [How to Install Windows PowerShell 4.0](https://social.technet.microsoft.com/wiki/contents/articles/21016.how-to-install-windows-powershell-4-0.aspx#Windows_Management_Framework_4_supportability_matrix)

