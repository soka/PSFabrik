# Clases y objetos 

La programación orientada a objetos implica **encapsular valores relacionados y sus funcionalidades en "**. Las clases **permiten abstraer los datos y sus operaciones asociadas como una caja negra**.

Una **clase es una plantilla para la creación de objetos** de datos según un modelo predefinido. Las clases se utilizan para representar entidades o conceptos, como los sustantivos en el lenguaje. Cada clase es un modelo que define un conjunto de variables -el estado, y métodos apropiados para operar con dichos datos -el comportamiento. Cada objeto creado a partir de la clase se denomina instancia de la clase.

Las clases se componen de elementos, llamados genéricamente «miembros», de varios tipos:

* Campos de datos: almacenan el estado de la clase por medio de variables, estructuras de datos e incluso otras clases. Los datos pueden estar almacenados en variables.
* Métodos: subrutinas de manipulación de dichos datos.

* [Clase (informática) - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Clase_(inform%C3%A1tica)).

# Inicio 
 
Una de las grandezas de **PowerShell** es que guarda la información en su tipo nativo: Los objetos. 

Todo el contenido de Windows PowerShell es un objeto. Incluso una simple cadena como "nombre" es un objeto, del tipo System.String. Puede canalizar cualquier objeto a [Get-Member](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/get-member?view=powershell-6) para ver su nombre de tipo (es decir, el tipo de objeto es) así como sus miembros, que incluye sus propiedades y métodos:

```
$var = 'Hello'
$var | Get-Member
```

Podemos hacer que esta larga lista de información sea más fácil de usar mediante el filtrado de los elementos que quiere ver. El comando Get-Member permite **mostrar solo los miembros que son propiedades**.

```
$var | Get-Member -MemberType Properties
```

*Los valores permitidos de MemberType son AliasProperty, CodeProperty, Property, NoteProperty, ScriptProperty, Properties, PropertySet, Method, CodeMethod, ScriptMethod, Methods, ParameterizedProperty, MemberSet y All.*

# Usar variables para almacenar objetos

```
$loc = Get-Location
$loc | Get-Member
```

Puede usar `Get-Member` para mostrar información sobre el contenido de las variables. `Get-Member` muestra que `$loc` es un objeto `PathInfo`, al igual que la salida de `Get-Location`.

# Objetos personalizados



# Enlaces externos 

* [Ver la estructura del objeto Get Member | Microsoft Docs](https://docs.microsoft.com/es-es/powershell/scripting/getting-started/cookbooks/viewing-object-structure--get-member-?view=powershell-6).
* [PowerShell: Objects and Output | IT Pro](https://www.itprotoday.com/management-mobility/powershell-objects-and-output).
* [Working with Values and Variables in PowerShell -- Microsoft Certified ...](https://mcpmag.com/articles/2012/10/02/pshell-values-variables.aspx).
* [PowerShell: Getting Started - Creating Custom Objects — The Ginger ...](https://www.gngrninja.com/script-ninja/2016/6/18/powershell-getting-started-part-12-creating-custom-objects).
* [How to use PowerShell Objects and Data Piping - Varonis Blog](https://blog.varonis.com/how-to-use-powershell-objects-and-data-piping/).
* [PowerShell variable properties: Description, Visibility, Options ...](https://4sysops.com/archives/powershell-variable-properties-description-visibility-options-attributes/).
* [Chapter 6. Working with Objects - Master-PowerShell | With Dr. Tobias ...](http://community.idera.com/powershell/powertips/b/ebookv2/posts/chapter-6-working-with-objects).