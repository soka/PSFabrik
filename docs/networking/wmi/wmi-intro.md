[TOC]


# Windows Management Instrumentation (WMI)

**WMI** es la implementación por parte de **Microsoft** de la iniciativa industrial llamada [**WBEM**](https://en.wikipedia.org/wiki/Web-Based_Enterprise_Management) (Web-Based Enterprise Management). Esta iniciativa es el fruto de un trabajo común en el seno del consorcio llamado **DMTF** (Distributed Management Task Force) compuesto por empresas tales como: HP, IBM, EMC, Cisco, Oracle, Microsoft y otras muchas. Otros sistemas operativos como Apple, Red Hat o Ubuntu implementan su versión de **WBEM**.

**WBEM** es un conjunto de tecnologías de gestión que se basa en los estándares abiertos de Internet cuyo objetivo es **unificar la gestión de plataformas y tecnologías diversas en un medio distribuido**.

**WBEM** y **WMI** se basan igualmente en otro estándar, el **CIM** (Common Information Model), que define los sistemas, las aplicaciones, las redes, los dispositivos y cualquier otro componente administrado.

![](images/Windows-Management-Instrumentation-WMI.png) 

**WMI** es una infraestructura para gestionar la información de equipos MS Win, es especialmente útil para obtener información de equipos remotos usando [DCOM](https://es.wikipedia.org/wiki/Modelo_de_Objetos_de_Componentes_Distribuidos) (Distributed Component Object Model), **WMI** funciona como una base de datos ofreciendo una variedad de información útil para el monitoreo de los sistemas.

**WMI** implementa clases para consultas sobre hardware, software o el propio SO. La aplicación WmiPrvSE.exe (C:\Windows\System32\wbem) ejecuta el proceso "WMI provider host", es la encargada de proporcionar la información de un equipo. Múltiples instancias de wmiprvse.exe podrían estar ejecutándose bajo diferentes nombres de usuarios: LocalSystem, NetworkService, o LocalService.

Arquitectura **WMI**:

![](images/arquitectura-wmi.PNG) 

El **consumidor WMI** representa el cliente que realiza la llamada (un script Powershell o una aplicación C# por ejemplo). 	 

Un **recurso administrado** puede ser cualquier componente físico o lógico que pueda administrarse vía WMI como los registros de eventos, procesos o un disco duro por ejemplo.

Un recurso administrado dialoga con la infraestructura WMI exclusivamente a través de un **proveedor WMI**. Cada recurso o más bien cada clase de recursos se describe en un archivo de texto en formato (Managed Object Format). Este fichero contendrá todas las propiedades, métodos y otras informaciones útiles para describir todo lo que será posible hacer en un recurso administrado a través de WMI.

Para que pueda ser utilizado por la infraestructura WMI, un archivo MOF debe estar compilado; esto cargará la definición del recurso en la base de datos CIM (los archivos .mof se encuentran en `%windir%\System32\wbem`).

Las **clases WMI** se almacenan en formato binario en la **base de datos CIM** (proceso realizado por la compilación de archivos MOF). Un ejemplo de clase podría ser la clase `Win32_Service` para gestionar servicios de Windows.

Las clases **WMI** se agrupan en espacios de nombres de forma jerarquica donde `\root` es el raiz. 


# WMI command-line (WMIC)

[**WMIC**](https://docs.microsoft.com/es-es/windows/desktop/WmiSdk/wmic) es una utilidad que provee un interface de línea de comandos para **WMI**.  

Por ejemplo para obtener los procesos ejecutándose en la máquina local:

```
C:\> WMIC PROCESS LIST BRIEF 
```

**WMIC** implementa un lenguaje de consulta similar a SQL llamado **WQL** (WMI Query Language), por ejemplo el siguiente comando obtiene algunas propiedades de la impresora predeterminada:

```
C:\> WMIC PRINTER WHERE default="TRUE" GET DriverName,PortName,PrintProcessor
```

# wbemtest

**wbemtest.exe** es una aplicación nativa de Windows localizada en `%windir%\System32\wbem`, conetamos con un espacio de nombres (por ejemplo "root\cimv2") y usamos la opción "Clases enumeradoras" para listar todas las clases.

![](images/wbemtest-1.PNG)

Podemos acceder a las propiedades y características de una clase determinada:

![](images/wbemtest-2.PNG)

# Obtener espacios de nombres 

Primero vamos a enumerar todos los espacios de nombre que tenemos disponibles:

```
PS C:\> Get-WmiObject -Class __Namespace -Namespace root  | select name

name
----
subscription
DEFAULT
CIMV2
msdtc
Cli
SECURITY
SecurityCenter2
RSOP
PEH
HP
StandardCimv2
WMI
directory
Policy
Interop
Hardware
ServiceModel
SecurityCenter
Microsoft
aspnet
Appv
```

Por defecto Get-WmiObject utiliza el espacio de nombres CIMV2. 

# Mi primer cliente WMI con PS

El siguiente cmdlet obtiene el nombre de host de la máquina local usando la clase `Win32_ComputerSystem`. 

```powershell
PS> Get-WmiObject Win32_ComputerSystem | Format-Table "Name"
```

# Computadoras remotas

El mecanismo de comunicación a distancia Windows PowerShell de la versión 2 de PowerShell se basa en el protocolo WS-MAN, también llamado «Administración de los servicios Web» o también «Administración remota de Windows (**WinRM**)».

WinRM, que quiere decir Windows Remote Management, permite enviar peticiones de administración a máquinas por red vía los puertos 5985 (HTTP) /5986 (HTTPS). 

Las conexiones remotas WMI se ven afectadas por el Firewall de Windows, por la UAC (User Account Control) y la configuración DCOM. En el Firewall de Windows debemos permitir explicitamente el tráfico WMI. Debemos ser miembro del grupo Administradores del ordenador remoto o ser miembro del grupo Administradores del dominio.

Siguiendo el ejemplo anterior podemos usar la clase `Win32_OperatingSystem` con un equipo remoto para obtener información del SO.

```
PS> $strComputer = "Computer_B"
PS> Get-WmiObject -Class Win32_OperatingSystem -ComputerName $strComputer -Namespace "root\cimv2" | format-list Caption, Version
```

# Obtener información

# Aplicaciones comerciales

## NetCrunch WMI Tool (adremsoft)

Accede a la información WMI local o remota y la muestra de forma gráfica dividida en secciones: Información general, procesos en ejecución, servicios de Windows, log de eventos del sistema, hardware, sistema operativo y un explorador WMI. La información se presenta de forma muy clara en cada sección.

[Enlace](https://www.adremsoft.com/wmitools/)

![](images/netcrunch-wmi-tool.PNG)

## Paessler's PRTG Network Monitor 

[WMI Tester](https://www.es.paessler.com/tools/wmitester) es un ejecutable que no requiere instalación.

![](images/paessler-1.PNG)

## WMI Explorer 2.0.0.2

[Repositorio en GitHub](https://github.com/vinaypamnani/wmie2).

![](images/wmi-explorer-2.0.0.2.PNG) 

## MoW PowerShell WMI Browser

Escrito ya hace algunos años este proyecto ya no está mantenido por su autor pero aún se puede descargar ([enlace](https://jdhitsolutions.com/blog/powershell/2848/wmi-explorer-from-the-powershell-guy/)) y funciona. 

![](images/mow-powershell-explorer.PNG) 

# Enlaces externos

* [About WMI](https://docs.microsoft.com/en-us/windows/desktop/wmisdk/about-wmi). 
* [Win32_ComputerSystem class](https://docs.microsoft.com/en-us/windows/desktop/cimwin32prov/win32-computersystem).
* [Best WMI Tools & Software for Windows Management Instrumentation Administration & Monitoring](https://www.ittsystems.com/wmi-tools-and-software/).
* [Setting up a Remote WMI Connection](https://docs.microsoft.com/en-us/windows/desktop/wmisdk/connecting-to-wmi-remotely-starting-with-vista).
* [WMI Tasks for Scripts and Applications](https://docs.microsoft.com/en-us/windows/desktop/wmisdk/wmi-tasks-for-scripts-and-applications).
* [Creating a WMI Script](https://docs.microsoft.com/en-us/windows/desktop/wmisdk/creating-a-wmi-script).
* [Creating WMI Clients](https://docs.microsoft.com/en-us/windows/desktop/wmisdk/creating-wmi-clients).
* [Using WMI](https://docs.microsoft.com/en-us/windows/desktop/wmisdk/using-wmi).
* [WMI Reference](https://docs.microsoft.com/en-us/windows/desktop/wmisdk/wmi-reference).
* [WMIC.exe](https://ss64.com/nt/wmic.html).