[TOC]

# Chequear si un host está en red (Win32_PingStatus class)

## Introducción

`Win32_PingStatus` es una clase **WMI** (Windows Management Instrumentation), para acceder a estas clases usamos el cmdlet `Get-WmiObject` (alias : `gwmi`), podemos obtener un listado de todas las clases del equipo local con este cmdlet:

```
PS> Get-WmiObject -List
```

Si queremos obtenerla de un equipo remoto especificamos el equipo añadiendo `-ComputerName 192.168.1.29` al cmdlet.

Buscamos la clase que nos interesa para este propósito 

```
PS> Get-WmiObject -List | where {$_.name -Match "Ping"}
```

## Ping básico 

Vamos a elaborar un ejemplo super sencillo para testear la conectividad en red con otra máquina.

Fuente: [Ping-Basic.ps1](../src/networking/monitor/Ping-Basic.ps1)

```
$IpAddr = "Address='8.8.8.8'"
# Podemos usar Get-WmiObject o su alias gwmi 
Get-WmiObject Win32_PingStatus -filter $IpAddr
```

Ejecución:

```
PS> .\Ping-Basic.ps1

Source        Destination     IPV4Address      IPV6Address                              Bytes    Time(ms)
------        -----------     -----------      -----------                              -----    --------
M01166        8.8.8.8         8.8.8.8                                                   32       39
```

## Código de estado

**StatusCode**: Un código 0 significa que la máquina ha respondido correctamente al ping, cualquier otro valor significa que se ha producido un problema. Vamos a ampliar el cmdlet inicial para acceder a las propiedad del objeto resultante de la llamada `Get-WmiObject Win32_PingStatus`.  

Fuente: [Ping-SatusCode.ps1](../src/networking/monitor/Ping-SatusCode.ps1)

```
$IpAddr = "Address='8.8.8.8'"
$PingRes = Get-WmiObject Win32_PingStatus -filter $IpAddr

$SttCode = $PingRes.StatusCode
$Addr = $PingRes.Address

if ($SttCode -eq 0) {
	"$Addr status $SttCode OK"
} else {
	"Error"
}
```

## Hacer ping a varias máquinas

Podemos hacer ping a más de una máquina encadenando varios host en una sentencia or por ejemplo:  

```
PS> Get-WmiObject Win32_PingStatus -filter "Address='google.com' or Address='trello.com'"

Source        Destination     IPV4Address      IPV6Address                              Bytes    Time(ms)
------        -----------     -----------      -----------                              -----    --------
M01166        google.com      172.217.17.14                                             32       39
M01166        trello.com      104.83.74.177                                             32       44
```

Para que sea extensible vamos a mejorarlo un poco, 

Fuente: [Ping-Multiple.ps1](../src/networking/monitor/Ping-Multiple.ps1)


## Rango de IPs 

Ahora vamos a hacer ping contra un rango de IPs (por ejemplo de 8.8.8.1 a 8.8.8.254), usando un loop `do..until` controlamos el contador `$i` que va generando el último octeto de la IP, guardamos el objeto resultante de la llamada `Get-WmiObject Win32_PingStatus` en una variable y sólo mostramos la dirección IP que estamos probando y el código de estado del ping resultante. 

Fuente: [Ping-SubNet-basic.ps1](/src/networking/monitor/Ping-SubNet-basic.ps1)

```
$i =1
$AddrIP = "8.8.8."
do { 
	$Ip4th = $AddrIP + $i	
	$PingRes = Get-WmiObject Win32_PingStatus -f "Address='$Ip4th'" 
	$PingRes | Format-Table Address, StatusCode -hideTableHeaders -auto; $i++
} until ($i -eq 254)
```


## Códigos de estado del comando ping

* 0 	Success
* 11001 	Buffer Too Small
* 11002 	Destination Net Unreachable
* 11003 	Destination Host Unreachable
* 11004 	Destination Protocol Unreachable
* 11005 	Destination Port Unreachable
* 11006 	No Resources
* 11007 	Bad Option
* 11008 	Hardware Error
* 11009 	Packet Too Big
* 11010 	Request Timed Out
* 11011 	Bad Request
* 11012 	Bad Route
* 11013 	TimeToLive Expired Transit
* 11014 	TimeToLive Expired Reassembly
* 11015 	Parameter Problem
* 11016 	Source Quench
* 11017 	Option Too Big
* 11018 	Bad Destination
* 11032 	Negotiating IPSEC
* 11050 	General Failure

# Test-NetConnection

[`Test-NetConnection`](https://docs.microsoft.com/en-us/powershell/module/nettcpip/test-netconnection?view=win10-ps) 

```
PS C:\> Test-NetConnection -ComputerName "google.com" -InformationLevel "Detailed"

ComputerName           : google.com
RemoteAddress          : 172.217.21.78
NameResolutionResults  : 172.217.21.78
InterfaceAlias         : Wi-Fi
SourceAddress          : 192.168.221.101
NetRoute (NextHop)     : 192.168.221.1
PingSucceeded          : True
PingReplyDetails (RTT) : 53 ms
```

# Sockets .Net



# Enlaces externos

* ["Retrieving a WMI Class"](https://docs.microsoft.com/es-es/windows/desktop/WmiSdk/retrieving-a-class).
* ["Win32_PingStatus class - Microsoft Docs"](https://docs.microsoft.com/en-us/previous-versions/windows/desktop/wmipicmp/win32-pingstatus).
* ["PowerShell Win32_PingStatus StatusCode IP Address Get-WmiObject ..."](https://www.computerperformance.co.uk/powershell/win32-pingstatus/).
* ["Pinging from PowerShell - LizardSystems"](https://lizardsystems.com/articles/pinging-powershell/).
* ["Pinging With WMI - PowerTheShell"](http://www.powertheshell.com/pinging-with-wmi/).
* ["Final Super-Fast Ping Command - Power Tips - PowerTips - IDERA ..."](https://community.idera.com/database-tools/powershell/powertips/b/tips/posts/final-super-fast-ping-command).
* ["#PSTip Resolving IP addresses with WMI – PowerShell Magazine"](https://www.powershellmagazine.com/2013/01/07/pstip-resolving-ip-addresses-with-wmi/).
* ["Test-Connection: Ping Remote Hosts the PowerShell Way"](https://www.adamtheautomator.com/test-connection-ping-powershell/).
* ["How To Create A Simple PowerShell Script For Server Monitoring"](https://blog.ipswitch.com/how-to-create-simple-server-monitors-with-powershell).
* ["Network Monitoring Powershell Script"](http://www.builtbymike.ca/information-technology-blog/powershell-network-monitoring-script/).
* ["PSNetMon - PowerShell Network Resource Monitoring Utility"](https://gallery.technet.microsoft.com/PSNetMon-PowerShell-cd2b345e).
* ["Network Device Status Monitor v1.0"](https://gallery.technet.microsoft.com/scriptcenter/Network-Device-Status-088f7b00).
* ["Remote Network Capture Utility v2.01"](https://gallery.technet.microsoft.com/Remote-Network-Capture-8fa747ba).