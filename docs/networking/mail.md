

```powershell
$From = “tudirecciondecorreo@gmail.com”
$To = “direcciondedestino@dominio.com”
$Cc = “direccionconcopia@dominio.com”
$Attachment = “C:\users\Username\Documents\nombrearchivo.extension”
$Subject = “Asunto del mensaje”
$Body = “Texto del mensaje”
$SMTPServer = “smtp.gmail.com”
$SMTPPort = “587”
Send-MailMessage -From $From -to $To -Cc $Cc -Subject $Subject -Body $Body -SmtpServer $SMTPServer -port $SMTPPort -UseSsl -Credential (Get-Credential) -Attachments $Attachment –DeliveryNotificationOption OnSuccess
```

# Mandar un correo usando Gmail

```
$email = "myemail3@gmail.com" 
 
$pass = "MyPass@123" 
 
$smtpServer = "smtp.gmail.com" 
 
 
$msg = new-object Net.Mail.MailMessage 
$smtp = new-object Net.Mail.SmtpClient($smtpServer) 
$smtp.EnableSsl = $true 
$msg.From = "$email"  
$msg.To.Add("$email") 
$msg.BodyEncoding = [system.Text.Encoding]::Unicode 
$msg.SubjectEncoding = [system.Text.Encoding]::Unicode 
$msg.IsBodyHTML = $true  
$msg.Subject = "Test mail from PS" 
$msg.Body = "<h2> Test mail from PS </h2> 
</br> 
Hi there 
"  
$SMTP.Credentials = New-Object System.Net.NetworkCredential("$email", "$pass"); 
$smtp.Send($msg)
```

# Enlaces externos

* [Cómo enviar un correo electrónico desde la línea de comandos](https://www.adslzone.net/como-enviar-correo-electronico-desde-linea-comandos/).
* [How to Send an Email Using Windows PowerShell](https://www.makeuseof.com/tag/send-email-windows-powershell/).
* [Powershell to send mail using gmail smtp settings](https://gallery.technet.microsoft.com/scriptcenter/Powershell-to-send-mail-97cc8278).