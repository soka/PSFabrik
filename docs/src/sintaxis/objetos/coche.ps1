#Clase coche con propiedades
class Coche
{ 
  $Marca
  $Modelo
  $Color
}
 
#Crear objeto coche de la clase Coche
$coche=New-Object -TypeName Coche

#Otra forma de crear objeto coche de la clase Coche
#$coche=[Coche]::new()

$coche.Marca = 'Renault'
$coche.Marca

$coche.Color = 'Rojo'
$coche.Color

$coche | Write-Host

$coche | Get-Member

$coche | Select-Object -Property *

$micolor = $coche.Color
# NO funciona muestra "Coche marca coche.Marca"
Write-Host "Coche marca $coche.Marca"

Write-Host "Coche marca $($coche.Marca)"

Write-Host "Coche color $micolor "