$Cuadrado = 2*2

$segundosPorDia = 24 * 60 * 60
# Funciona también
#$segundosPorDia = 24*60*60
$segundosPorDia

$semanasPorAnio = 365 / 7

$myNum = 4+4

$myNum = 1
$myNum = $myNum + 1
# es equivalente a 
$myNum++

# Esto produce error
#$myNum**
$myNum=2
$myNum*=2
$myNum
# Igual a 4

# Equivalente a  $myNum = $myNum*2 
$myNum*=2 
$myNum+=2 

