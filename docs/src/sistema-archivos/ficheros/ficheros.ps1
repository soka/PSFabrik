$txt = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut enim in libero vehicula viverra. Ut at enim quis justo lacinia euismod nec eget urna. Proin pharetra fringilla dolor, a auctor ligula rutrum aliquam. Duis molestie justo nec lacus varius, sed hendrerit elit tincidunt."

Set-Content -Path .\test1.txt -Value $txt
Add-Content -Path .\test2.txt -Value $txt
Out-File -FilePath .\test3.txt -InputObject $txt

# Usamos un pipe
Get-Process | Out-File -FilePath .\test4.txt

# Set-Content no funcionaria con comando de arriba 
Get-Process | Out-String | Set-Content -Path .\test6.txt

# Retorna propiedad del fichero, modo, última modificación, tamaño y nombre 
Get-ChildItem -Path .\test1.txt 
# Vacia contenido 
Clear-Content -Path .\test1.txt 

"texto de prueba" | Add-Content -Path .\test3.txt

(Get-Content .\test3.txt).replace('prueba', 'MyValue') | Set-Content .\test3.txt