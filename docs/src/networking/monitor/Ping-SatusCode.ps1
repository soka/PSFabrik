$IpAddr = "Address='8.8.8.8'"
$PingRes = Get-WmiObject Win32_PingStatus -filter $IpAddr

$SttCode = $PingRes.StatusCode
$Addr = $PingRes.Address

if ($SttCode -eq 0) {
	"$Addr status $SttCode OK"
} else {
	"Error"
}
