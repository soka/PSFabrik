$hosts = 'google.com', 'trello.com', 'es.wikipedia.org'

$filter = 'Address="' + ($hosts -join '" or Address="') + '"'

Get-WmiObject -Class Win32_PingStatus -Filter $filter |
  Select-Object -Property Address, ProtocolAddress, ResponseTime
