$i =1
$AddrIP = "8.8.8."
do { 
	$Ip4th = $AddrIP + $i	
	$PingRes = Get-WmiObject Win32_PingStatus -f "Address='$Ip4th'" 
	$PingRes | Format-Table Address, StatusCode -hideTableHeaders -auto; $i++
} until ($i -eq 254)