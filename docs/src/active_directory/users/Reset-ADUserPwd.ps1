import-module activedirectory

Write-Host "Cree una contraseña segura usando un servicio como https://passwordsgenerator.net/"
$UserId = Read-Host "Deme Identidad del usuario (ejemplo: 'b.sinclair')"

$Newpwd = Read-Host "Deme nueva clave (compleja)" 

Set-ADAccountPassword $UserId -NewPassword (ConvertTo-SecureString -AsPlainText -String "$Newpwd" -force)

$YesNoResp = Read-Host "Quiere que deba establecer una nueva en primer acceso? [s/n]"

if ( $YesNoResp  -eq 's' ) {
	Write-Host "Estableciendo que deba definir nueva clave en primer login"
	Set-ADUser -Identity $UserId -ChangePasswordAtLogon $true
} 


