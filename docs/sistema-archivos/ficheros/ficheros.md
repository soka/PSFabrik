[TOC]

# Manipulación de texto con PowerShell

Existen una serie de cmdlets para para manipular ficheros de texto `Add-Content`, `Clear-Content`, `Get-Content` y `Set-Content`. Además `Out-File` puede crear un fichero de texto o escribir contenido.

# Creando un fichero

Se pueden usar ambas, `Add-Content` y `Set-Content`, pero necesitan que se indique el contenido de texto.

```PS
$txt = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut enim in libero vehicula viverra. Ut at enim quis justo lacinia euismod nec eget urna. Proin pharetra fringilla dolor, a auctor ligula rutrum aliquam. Duis molestie justo nec lacus varius, sed hendrerit elit tincidunt."

Set-Content -Path .\test1.txt -Value $txt
Add-Content -Path .\test2.txt -Value $txt
Out-File -FilePath .\test3.txt -InputObject $txt
```

Podemos usar `Out-File` con un _pipe_, en este caso obtenemos los procesos en ejecución de la maquina local y redirigimos la salida con información a un fichero de texto:

```PS
Get-Process | Out-File -FilePath .\test4.txt
```

Si tratamos de hacer lo mismo con el cmdlet `Set-Content` no funciona, debemos convertir los datos en cadena previamente con `Out-String`:

```PS
Get-Process | Out-String | Set-Content -Path .\test6.txt
```

# Vaciando el contenido

A veces es necesario conocer los datos del fichero, `Get-ChildItem` retorna modo, última modificación, tamaño y nombre:

```PS
Get-ChildItem -Path .\test1.txt
```

Vaciar el contenido es muy sencillo con `Clear-Content`, además preserva los permisos del fichero:

```PS
Clear-Content -Path .\test1.txt
```

# Modificando el contenido

```PS
"texto de prueba" | Add-Content -Path .\test3.txt
```

# Reemplazando contenido

```PS
(Get-Content .\test3.txt).replace('prueba', 'MyValue') | Set-Content .\test3.txt
```

# Enlaces externos

- ["Hone your PowerShell text manipulation skills"](https://searchwindowsserver.techtarget.com/feature/Editing-content-with-Windows-PowerShell).
