# Obtener ayuda 

## Get-Help [help]

`Get-Help` proporciona la información necesaria sobre cmdlets, comandos, funciones, scripts y flujos de trabajo de PowerShell.

```
Get-Help Get-Process
```

## Get-Command [gcm]

`Get-Command` Muestra la lista de comandos de una función específica o para un propósito específico basado en tu parámetro de búsqueda.

```
Get-Command *-service*
```

Muestra comandos que llevan “-service” en su nombre (`New-Service`,`Restart-Service`,...). Recuerda utilizar los asteriscos en ambos lados de la consulta porque es un comodín que ayuda a buscar lo desconocido.


# Personalizando el aspecto del interprete de comandos

## Titulo de la ventana

```  
$Host.UI.RawUI.WindowTitle="Hola mundo!"
```

## Modificar los colores

Es posible cambiar el color de fondo de la consola de PS:

```
# Establece el color de fondo de la consola
$Host.UI.RawUI.BackgroundColor="magenta"
# Establece el color de la letra 
$Host.UI.RawUI.ForegroundColor="blue"
```

`$Host` es una variable predefinida del sistema que hace referencia al objeto consola, podemos modificar las propiedades del objeto.

**Enlaces externos**

* [PowerShell For Fun: Do You Know There is a $Host Variable](https://mshforfun.blogspot.com/2006/08/do-you-know-there-is-host-variable.html).
* [PowerShell Basics: Console Configuration | IT Pro](https://www.itprotoday.com/management-mobility/powershell-basics-console-configuration).


## Tamaño de ventana

El *buffer size* indica la altura y anchura mientras que *window size* es la porción del buffer visible. Existe una limitación, el *window size* debe ser siempre menor que el *buffer size*. 

Antes de modificar nada podemos consultar los valores iniciales:

```
$Host.UI.RawUI.BufferSize
# Width = 192,  height = 3000

$Host.UI.RawUI.WindowSize
# Width = 111,  height = 33
```

La anchura se mide en carácteres (no en pixels) y la altura en líneas.

Si redimensionamos la ventana del interprete PS podemos ver como `$Host.UI.RawUI.WindowSize` cambia sus valores. También podemos guardar los valores iniciales en una variable como un objeto y acceder a sus propiedades para modificar los valores.

```
$size = $Host.UI.RawUI.WindowSize
$size.Width = 100
$size.Height = 25
$Host.UI.RawUI.WindowSize = $size
```

Estas y otras propiedades también se pueden modificar usando las propiedades de la venta usando el GUI de MS Win.

## Perfil de usuario

Las modificaciones visuales realizadas arriba son temporales hasta que cerramos el interprete. Si queremos que los cambios sean permanentes debemos modificar el script del perfil que se ejecuta cada vez que abrimos el interprete y donde podemos guardar nuestras preferencias. 

Para averiguar donde está ubicado este script escribimos (ISE tiene su propio script de configuración):  

```
$profile
# En mi caso: C:\Users\i.landajuela\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1
```

Si el fichero no existe podemos crearlo a mano y añadir los cmdlets y comandos que queramos cargar con el arranque del interprete. 


# Archivos de secuencia de comandos de Windows PowerShell

Un archivo de secuencia de comandos de Windows PowerShell no es más que un archivo de texto que tiene una.Extensión de nombre de archivo PS1

Invoke-Expression proporciona una forma de ejecutar un script desde Windows PowerShell.

```
Invoke-Expression c:\scripts\prueba.ps1
```

Aunque la creación y ejecución de scripts (que tienen la extensión “ps1“) en Windows PowerShell es posible; existen restricciones por motivos de seguridad. Así que puedes cambiar el nivel de seguridad utilizando el comando Set-ExecutionPolicy.

Puedes escribir Set-ExecutionPolicy seguido de uno de los cuatro niveles de seguridad:

* Restricted.
* Remote Signed
* All Signed
* Unrestricted. 

Por ejemplo, puedes asignar el estado de política restringida usando:


Ejecutar un archivo ps1 desde un archivo .bat:

``` 
"C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -noLogo -ExecutionPolicy unrestricted -file "C:\Users\i.landajuela\Documents\BarraHerramientasWin10\Aplicaciones\PowerShell\SesionRemotaPS-SERVER.ps1"
```

# Comillas

Como práctica recomendada, utilice comillas para delimitar una variable a menos que tenga una razón concreta para realizar lo contrario. Hay tres casos específicos donde se desea utilizar comillas dobles.

La primera es cuando se necesita insertar contenido de una variable en una cadena. Dentro de comillas dobles, Windows PowerShell buscará el $ y supondrá que es todo tras el $, hasta el primer carácter que no es válido en un nombre de variable, un nombre de variable. El contenido de esa variable reemplazará el nombre de variable y los $:

```
$name = 'Don'
$prompt = "My name is $name"
$prompt

# Esto produce como salida My name is $name
$prompt = 'My name is $name'
```

Comillas dobles cuando una cadena debe contener comillas simples:

```
$filter1 = "name='BITS'"
$filter1
#name='BITS'

$computer = 'BITS'
$filter2 = "name='$computer'"
$filter2
#name='BITS'
```

# Carácteres de escape

Tabulador:

```
$head = "Column`tColumn`tColumn"
```

Otro ejemplo, la primera $ está siendo "escapado." Quita su significado especial como un descriptor de acceso variable

```
$computer = "PC-WORK"
$debug = "`$computer contains $computer"
$debug
# Imprime $computer contains PC-WORK
```

# Limpiar la consola 

La función [`Clear-Host`](https://technet.microsoft.com/es-es/library/hh852689.aspx) limpia el contenido de la pantalla: 

```
Clear-Host
```

# Variables del sistema 

El cmdlet `Get-Variable` muestra las variables del sistema de PowerShell (por ejemplo `PSVersionTable` que usamos al principio para determinar la versión de PS que estamos usando). 

Puede usar el cmdlet Remove-Variable para quitar variables, que no se controlan mediante PowerShell, de la sesión actual. Escriba el siguiente comando para borrar todas las variables:

```
Remove-Variable -Name * -Force -ErrorAction SilentlyContinue
```

# Invocar comandos

Cuando desees ejecutar un comando o un script de PowerShell, local o remotamente en una o varias computadoras, “Invoke-Command”:

```
Invoke-Command -ScriptBlock {Get-EventLog system -Newest 50}
```

O en una computadora remota usando el parámetro `-ComputerName Server01`.


# Alias

Un alias sirve para dar un nombre alternativo a un comando. 

```
New-Item alias:np -value c:\windows\system32\notepad.exe
Remove-Item alias:np
```

# Auto compleción de comandos

Podemos usar la tecla TAB para autocompletar por ejemplo la ruta de una carpeta.


# Enlaces externos

* [Windows PowerShell: Curso intensivo sobre secuencias de comandos](https://technet.microsoft.com/es-es/library/hh551144.aspx).
* [Introducción a PowerShell - La Escuela del Programador](http://respag.net/new-page-2.aspx).
* [Introducción a la línea de comandos de Windows con PowerShell](https://programminghistorian.org/es/lecciones/introduccion-a-powershell).
* [20 comandos de Windows PowerShell que debe conocer » EsGeeks](https://esgeeks.com/comandos-windows-powershell-con-ejemplos/).