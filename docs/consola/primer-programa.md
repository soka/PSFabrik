# Nuestro primer contacto "Hola Mundo!"

Vamos a escribir nuestro primer programa, como en el resto de lenguajes de programación comenzamos escribiendo la cadena "Hola Mundo!" por la consola:

```
Write-Host "Hola Mundo!"
```

[Write-Host](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/write-host?view=powershell-6) es un alias del viejo`echo`.


# Obtener la entrada del usuario


```
$name = Read-Host “What’s your name?”
Write-Host "Hello $name"
```



# Enlaces externos

* [Write-Host - Microsoft Docs](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/write-host?view=powershell-6).