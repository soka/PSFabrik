# MANDAR UN MENSAJE A UN GRUPO DE TELEGRAM USANDO UN BOT Y UN SCRIPT POWERSHELL

## Crear un Bot

Para empezar debemos crear un **Bot de Telegram**. Podemos usar el padre de todos los padres de los bots de Telegram llamado [**BoFather**](https://telegram.me/BotFather), usando el canal abierto BoFather nos guia a través de varias preguntas para que creemos nuestro bot personal.

![](/utilidades/telegram/img/01.png)

**Hablando con BoFather:**

Para crear un nuevo bot introducimos el siguiente comando:

```
/newbot
```

Una vez introducido nos solicitará un nombre para el nuevo bot. Después nos solicitará un nombre de usuario para el bot (la cadena debe acabat con 'bot'). Si se crea correctamente el bot es accesible como enlace Telegram `tg://resolve?domain=nombre_usuario_bot`

Cuando finalizamos el proceso BoFather nos proporciona un APT Token para acceder a la API HTTP.

![](/utilidades/telegram/img/06.png)

## Obtener el ID del chat de grupo

![](/utilidades/telegram/img/02.png)

Podemos usar el bot [@my_id_bot](https://t.me/my_id_bot) para obtener el ID de nuestro usuario personal, añadimos el bot a nuestro Telegram y escribimos dentro el siguiente comando:

```
@my_id_bot
```

La respuesta del comando debe ser un ID numérico como la parte tachada de la imagen inferior:

![](/utilidades/telegram/img/03.png)

**Como queremos mandar los mensajes a un grupo usando nuestro Bot** creamos un grupo (introducimos al bot) y obtenemos el ID del grupo de la URL de [Telegram Web](https://web.telegram.org/), es el número decimal a continuación de la URL `https://web.telegram.org/#/im?p=g` tachado en la imagen.

![](/utilidades/telegram/img/04.png)

## La magia del script PowerShell

Ahora ya tenemos todas las piezas necesarias:

* Un bot creado
* El token ID del bot para usar la API HTTP.
* Un grupo de Telegram con el bot dentro.
* El ID del grupo.

Usaremos el script obtenido del siguiente enlace de "Techno Chat Tech Blog!" (muy recomendable) ["How to use Telegram Bot as Event Notifier to send instant notification on your Smartphone using PowerShell"](https://technochat.in/2017/03/how-to-use-telegram-bot-as-event-notifier-to-send-instant-notification-on-your-smartphone-using-powershell/#more-3046) - 27/03/2017. 

![](/utilidades/telegram/img/05.png)

**Nota**: Dentro del script sólo debemos modificar la variable `$token = "bot_token"` por el Token que nos ha proporcionado BoFather cuando hemos creado el bot.

**Fuente:** [sendmessage_telegram.ps1](/src/utils/telegram/sendmessage_telegram.ps1).

```
#Usage: sendmessage_telegram.ps1 -chat_id CHAT_ID -text 'TEXT' -markdown
param(
[string]$chat_id = $(Throw "'-chat_id' argument is mandatory"),
[string]$text = $(Throw "'-text' argument is mandatory"),
[switch]$markdown,
[switch]$nopreview
)

$token = "bot_token"
if($nopreview) { $preview_mode = "True" }
if($markdown) { $markdown_mode = "Markdown" } else {$markdown_mode = ""}

$payload = @{
    "chat_id" = $chat_id;
    "text" = $text
    "parse_mode" = $markdown_mode;
    "disable_web_page_preview" = $preview_mode;
}

Invoke-WebRequest `
    -Uri ("https://api.telegram.org/bot{0}/sendMessage" -f $token) `
    -Method Post `
    -ContentType "application/json;charset=utf-8" `
    -Body (ConvertTo-Json -Compress -InputObject $payload)
```

Abrimos una consola de PS en MS Win y ejecutamos el cmdlet:

```
PS> .\sendmessage_telegram.ps1 -chat_id "-ID_chat_grupo" -text "Texto a enviar a grupo #Tests #PS #PowerShell #Scripting #cmdlet"
```

**Nota:** El ID del chat debe ir precedido del carácter '-'.


## Conclusiones

Las utilidades que le podemos dar a este sistema de mensajería son tantas como se nos ocurran, una muy obvia es monitorizar una computadora usando PS para medir indicadores de rendimiento: CPU, puertos y servicios abiertos, procesos cave ejecutandose, ocupación del disco duro,...

También se puede usar para enviar mensajes informativos a diferentes grupos de forma rápida (podemos implementar algun sistema de plantillas para mensajes tipo más comunes).

**Mejoras**: Se puede ampliar el script pasandole como parámetro un array de IDs de chats y así notificar a varios grupos de usuarios el mismo mensaje.

## Enlaces externos

**Telegram y PowerShell**:

* technochat.in ["How to use Telegram Bot as Event Notifier to send instant notification on your Smartphone using PowerShell"](https://technochat.in/tag/how-to-send-telegram-messages-from-powershell-script/).
* itdroplets.com ["Automating Telegram Messages with Powershell"](https://www.itdroplets.com/automating-telegram-messages-with-powershell/).

**Powershell**:

* [Get-Process - Microsoft Docs](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.management/get-process?view=powershell-6): Obtiene los procesos corriendo en una computadora.
* [PowerShell: Check if a process is running and if not start it | Robin ...](https://rcmtech.wordpress.com/2015/07/15/powershell-check-if-a-process-is-running-and-if-not-start-it/).
* [PowerShell - Check if process is running](http://babblecode.blogspot.com.es/2012/03/powershell-check-if-process-is-running.html).
* [Using PowerShell to verify if a remote TCP port is accessible ...](https://akawn.com/blog/2015/05/using-powershell-to-verify-if-a-remote-tcp-port-is-accessible/).
* [Travis Gan: Use PowerShell To Test Port](http://www.travisgan.com/2014/03/use-powershell-to-test-port.html).
* [Check for Free Space on your Drives - LazyAdmin PowerShell version ...](http://www.thirdtier.net/check-for-free-space-on-your-drives-lazyadmin-powershell-version/).
