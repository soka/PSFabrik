
Usamos el cmdlet [`Get-Wmiobject`](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.management/get-wmiobject?view=powershell-5.1) para preguntar a la clase [`Win32_LogicalDisk`](https://msdn.microsoft.com/en-us/library/aa394173(v=vs.85).aspx). Limitamos la consulta al disco del sistema C.  

```
PS> Get-WMIObject Win32_Logicaldisk -filter "deviceid='C:'"
```

![](/computadora/img/01.png)

Filtramos los resultados de salida con una tubería y el cmdlet `Select`:

```
PS> Get-WMIObject Win32_Logicaldisk -filter "deviceid='C:'" | Select PSComputername,DeviceID,Size,Freespace
```

Podemos convertir la salida a una tabla clave-valor (hash table).

```
PS> Get-WMIObject Win32_Logicaldisk -filter "deviceid='C:'" | Select PSComputername,DeviceID,@{Name="SizeGB";Expression={$_.Size/1GB -as [int]}},@{Name="FreeGB";Expression={[math]::Round($_.Freespace/1GB,2)}}
```

# Enlaces externos

* [Check for Free Space on your Drives - LazyAdmin PowerShell version ...](https://www.petri.com/checking-system-drive-free-space-with-wmi-and-powershell).
* [How to Create Simple Server Monitors with PowerShell - Ipswitch Blog](https://blog.ipswitch.com/how-to-create-simple-server-monitors-with-powershell).
* [Monitoring with PowerShell » ADMIN Magazine](http://www.admin-magazine.com/Archive/2015/26/Retrieving-Windows-performance-data-in-PowerShell).
* [CIM Instead of WMI to discover monitor info | Askme4Tech](http://askme4tech.com/cim-instead-wmi-discover-monitor-info).
