
Fuente: [Get-DaysOnline.ps1]()

```
$os = Get-WmiObject -Class Win32_OperatingSystem
$boottime = [System.Management.ManagementDateTimeConverter]::ToDateTime($os.LastBootupTime)
$timedifference = New-TimeSpan -Start $boottime
$days = $timedifference.TotalDays

Write-Host "El sistema esta funcionando desde hace $days dias."
```
