function Get-ComputerHardwareReport
{
  <# 
      .SYNOPSIS 
      Gathers basic systems information for a given computer. 
      .DESCRIPTION 
      Gathers Basic computer information (Make/Model/ShipDate/RAM/CPU/Warranty) and reports it back 
 
      NOTE: Model/ShipDate/Warranty information is only available if it is a DELL system and you have a Dell API key that you pass in. 
      .EXAMPLE 
      Get-DellServerHardwareReport -computer myserver-01 
  #>
  [CmdletBinding()]
  param
  (
    [Parameter(Mandatory=$false, Position=0)]
    [Object[]]
    $computer = (Read-Host which computer to check?),
    
    [Parameter(Mandatory=$false, Position=1)]
    [System.Object]
    $APIKey = $DellAPIKey
  )
  foreach ($thiscomputer in $computer) {
    if (Test-Connection -ComputerName $thiscomputer -Count 1 -ErrorAction SilentlyContinue) {
      $sysenc = (gwmi -Class win32_systemenclosure -ComputerName $thiscomputer)
      if ($sysenc.count -gt 1) {$sysenc = $sysenc[0]} #We only care to keep one service tag. Some Dell machines report 2 SerialNumbers for some reason
      $svctag = $sysenc.SerialNumber
      if ($sysenc.Manufacturer -like "*dell*") {
        $dellWarrantyInfo = Get-DellWarranty -AssetTag $svctag -APIKey $APIKey
        $shipDate = $dellWarrantyInfo[0].StartDate
        $model = $dellWarrantyInfo[0].ProductId
      }
      $cpu = Get-WmiObject -Class Win32_Processor -ComputerName $thiscomputer
    
      #region MEMORY
      # REF https://www.nextofwindows.com/getting-ram-info-on-local-or-remote-computer-in-powershell
      $memoryslot = Get-WmiObject Win32_PhysicalMemoryArray -ComputerName $thiscomputer
      $memory = Get-WMIObject Win32_PhysicalMemory -ComputerName $thiscomputer
      $memorymeasure = Get-WMIObject Win32_PhysicalMemory -ComputerName $thiscomputer | Measure-Object -Property Capacity -Sum
      
      #endregion MEMORY
    
      Write-Host ("Computer Information for ").PadRight(30) -NoNewline -ForegroundColor Cyan
      Write-Host $thiscomputer.ToUpper() -ForegroundColor Green
    
      Write-Host ("Computer Name: ").PadRight(30) -NoNewline
      Write-Host $sysenc.__SERVER -ForegroundColor Green
      
      Write-Host ("Model: ").PadRight(30) -NoNewline
      if ($sysenc.Manufacturer -like "*dell*") {Write-Host $model -ForegroundColor Green} ELSE {Write-Host 'Not Dell Equipment. Unable to Check.'}
        
      Write-Host ("Service Tag: ").PadRight(30) -NoNewline 
      Write-Host $svctag -ForegroundColor Green
    
      Write-Host ("Ship Date: ").PadRight(30) -NoNewline
      if ($sysenc.Manufacturer -like "*dell*") {Write-Host $shipDate.ToShortDateString() -ForegroundColor Green} ELSE {Write-Host 'Not Dell Equipment. Unable to Check.'}
    
      # Format and Print MEMORY
      Write-Host ""
      Write-Host "Memory Information: ".PadRight(32) -ForegroundColor Cyan
      Write-Host "Total memory slot available: ".PadRight(32)    -NoNewLine; Write-Host $memoryslot.MemoryDevices             -ForegroundColor Green
      Write-Host "Maximum Capacity allowed: ".PadRight(32)    -NoNewLine  ; Write-Host $($memoryslot.MaxCapacity/1024/1024)  -ForegroundColor Green -NoNewline; Write-Host "Gb" -ForegroundColor Green
      Write-Host "Total memory sticks installed: ".PadRight(32) -NoNewLine; Write-Host $memorymeasure.count                  -ForegroundColor Green
      Write-Host "Total RAM installed: ".PadRight(32)         -NoNewLine  ; Write-Host $($memorymeasure.sum/1024/1024/1024)  -ForegroundColor Green -NoNewline; Write-Host "Gb" -ForegroundColor Green
      Write-Host ""
    
      # Format and Print CPU
      Write-Host "CPU Information: ".PadRight(32) -ForegroundColor Cyan
      foreach ($c in $cpu) {
        Write-Host "$($c.SocketDesignation)".PadRight(10) -NoNewline
        Write-Host "$($c.Name)".PadRight(50)
      }
    
      Write-Host ""
      Write-Host "Overall Warranty Status:" -ForegroundColor Cyan
      if ($sysenc.Manufacturer -like "*dell*") {
        $dellWarrantyInfo | select ServiceLevelCode,ServiceLevelDescription,@{E={$_.EndDate.ToShortDateString()};L="EndDate"} | sort EndDate,ServiceLevelCode -Descending | ft -AutoSize
        #$dellWarrantyInfo | select ServiceLevelCode,ServiceLevelDescription,@{E={$_.EndDate.ToShortDateString() | Write-Host -ForegroundColor blue};L="EndDate"} | sort EndDate,ServiceLevelCode -Descending | ft -AutoSize
      } ELSE {Write-Host 'No Information. Detected system is not Dell and we can not check warranty state automatically.'}
    } ELSE {Write-Output "Computer $thiscomputer appears to be offline. No work has been performed."}
}}


#Get-ComputerHardwareReport -computer M01166

#$ComputerOS = Get-ADComputer M01166 -Properties OperatingSystem,OperatingSystemServicePack
#$ComputerOS

# https://gallery.technet.microsoft.com/scriptcenter/Hardware-Inventory-Using-fe6611e0
Get-WmiObject -ComputerName M01166  Win32_BaseBoard  |  Select Name,Manufacturer,Product,SerialNumber,Status

Get-WmiObject Win32_Battery -ComputerName M01166  | Select Caption,Name,DesignVoltage,DeviceID,EstimatedChargeRemaining,EstimatedRunTime 

# https://gist.github.com/BDollar/5455098	