# Variables de tipo string (cadena de texto)
$UserName = "Bob"
$UserName = 'Bob'

# Es valido 
$UserName = "Bob y Lisa"

$UserName
Write-Host $UserName

$UserName = "Lisa"

# OK
$User_Name = "Moises"
# Error 
#$User-Name = "Moises"
# Error 
#$User Name = "Moises"

#$UserName | get-member

$UserAge = 43
$UserAge = "Bob"
$UserAge

# OK
$UserAge = 43.4

# KO
#$UserAge = 43,5

# Si queremos establecer el mismo valor a varias variables
$a = $b = $c = 1
"These are the values of the variables: $a, $b, $c."

# Si queremos hacerlo con diferentes valores en una línea
$a, $b, $c = 1, 2, 3

# Obtener tipo de variable 
$b.GetType().Name

# PS define las variables $true y $false para los booleanos
$isChecked = $false
if (!$isChecked) { "No" }

	


